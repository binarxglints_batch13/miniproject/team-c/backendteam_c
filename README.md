# MINI PROJECT TEAM C

### MOVIE REVIEW APP

---

### Installation

First initiate the project directory to install required package using:

```bash
$ npm i -D
```

For development purpose:

```bash
$ npm run dev
```

For testing purpose:

```bash
$ npm run seed:test
$ npm run test
$ npm run seed:test:remove
```

---

### How To use

To use the API you must follow below
[POSTMAN Documentation](https://documenter.getpostman.com/view/16564652/TzsfnkdT "POSTMAN Documentation")
